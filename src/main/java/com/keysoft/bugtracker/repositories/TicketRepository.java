package com.keysoft.bugtracker.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysoft.bugtracker.domain.Ticket;

import io.swagger.annotations.Api;

@Api(tags = "Ticket Entity")
@RepositoryRestResource(path = "tickets")
public interface TicketRepository extends JpaRepository<Ticket, Integer> {
	@RestResource(path="descriptionIgnoreCaseContaining", rel="descriptionIgnoreCaseContaining")
	public List<Ticket> findByDescriptionIgnoreCaseContaining(String description);
	@RestResource(path="applicationId", rel="applicationId")
	public List<Ticket> findByApplicationId(Integer application_id);
    @RestResource(path = "titleIgnoreCaseContaining", rel = "titleIgnoreCaseContaining")
    public List<Ticket> findByTitleIgnoreCaseContaining(String title);
    @RestResource(path = "titleStartingWith", rel = "titleStartingWith")
    public List<Ticket> findByTitleStartingWith(String title);
    
}
